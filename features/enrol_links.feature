Feature: Automate course purchase links for FS courses and Programs
    As a user, I want to automate the course enrolment links for all the FS courses and Bundles

    @Regression_Test
    Scenario: Navigate to course catalogue page
        Given user is on online domain home page
        When they clicks the courses & degress link
        Then the user should get the course catalogue page

    @Regression_Test
    Scenario: Purchase Future skills courses
        Given the user is on course catalogue page
        When they clicks the FS courses
        And login into Marketplace
        Then they should be able to purchase the courses
        And the user able to remove the course from the cart

    @Regression_Test
    Scenario: Purchase Future skills bundles
        Given the user is on bundle page
        When they clicks the FS program
        Then they should be able to purchase the bundles
