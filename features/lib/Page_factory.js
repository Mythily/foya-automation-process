var object_factory = {
    //Drupal statging site
    drupal_staging_url: "https://online.marketstg.rmitonlinestg.com/",

    //Course catalogue URL
    course_page_url: "https://online.marketstg.rmitonlinestg.com/courses?category=short",

    //Bundle page URL
    bundle_page_url: "https://online.marketstg.rmitonlinestg.com/courses?category=bundle",

    // Courses and degress
    courses_degress: "Courses & degrees",  //locator - linkText

    //FS short courses
    fs_course_filter: "//span[contains(text(),'Future Skills short courses')]",  //locator - Xpath

    //FS first course 
    fs_first_course: "/html[1]/body[1]/div[1]/div[1]/div[1]/div[1]/main[1]/div[1]/div[1]/div[2]/div[2]/div[1]/div[2]/div[1]", //locator - Xpath

    //Course purchase link
    enrol_link: "Enrol now",  //locator - linkText

    //Login iframe
    iframe_field: "//iframe[@class='cLHC_AddToCart']", //locator - xpath

    //Delete course from cart
    delete_from_cart: "fa-trash",  //locator - classname

    //Course and Bundles lenght
    course_bundle_list: ".list-complete-item", //locator - css

    //Login field
    login_link: "//a[contains(text(),'login')]", //locator - xpath
    username: "Ecom_User_ID",  //locator - id
    passowrd: "Ecom_Password",  //locator - id
    loginBtn: "loginButton2", //locator - id
};

module.exports = object_factory;
