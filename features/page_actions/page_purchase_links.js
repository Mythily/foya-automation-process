let webdriver = require('selenium-webdriver');
let chrome = require('selenium-webdriver/chrome');
let assert = require('cucumber-assert');
const { Builder, By, Key, until } = require("selenium-webdriver");
const elements = require('../lib/Page_factory');
const data = require('../lib/Data_provider');
let driver;

driver = new webdriver.Builder().withCapabilities(webdriver.Capabilities.chrome()).build();

driver.manage().window().maximize();

driver.manage().deleteAllCookies();

async function homepage() {
    driver.get(elements.drupal_staging_url);
}

async function navigate_to_course_catalogue() {
    const courses_degress_field = await driver.wait(until.elementLocated(By.linkText(elements.courses_degress)), 5000);
    await courses_degress_field.click();
}

async function catalogue_page_validation() {
    try {
        catalogue_page_url = driver.getCurrentUrl();
    } catch (error) {

    }
}

async function short_course_filter() {
    const fs_filter_field = await driver.wait(until.elementLocated(By.xpath(elements.fs_course_filter)), 5000);
    await fs_filter_field.click();
}

async function firstCourse() {
    try {
        await driver.navigate().refresh();
        await driver.sleep(5000);
        const list = await driver.wait(until.elementLocated(By.xpath(elements.fs_first_course)), 5000);
        await list.click();
        const enrol_link_field = await driver.wait(until.elementLocated(By.linkText(elements.enrol_link)), 5000);
        await enrol_link_field.click();

    } catch (error) {

    }
}

async function frameSwitching() {
    await driver.switchTo().frame(driver.wait(until.elementLocated(By.xpath(elements.iframe_field)), 5000));

    await driver.sleep(5000);

    await login_function();

    await driver.sleep(20000);
    const saveButton = await driver.wait(until.elementLocated(By.className(elements.delete_from_cart)), 5000);
    await driver.wait(until.elementIsVisible(saveButton)).click();

    await driver.sleep(10000);

}

async function courseList() {
    await driver.get(elements.course_page_url);
    try {
        await driver.navigate().refresh();
        await driver.sleep(5000);
        var whole_div = await driver.findElements(By.css(elements.course_bundle_list));
    } catch (error) {

    }

    for (i = 2; i <= whole_div.length; i++) {
        await driver.get(elements.course_page_url);
        try {
            await driver.navigate().refresh();
            await driver.sleep(5000);
            const element = await driver.wait(until.elementLocated(By.xpath("/html[1]/body[1]/div[1]/div[1]/div[1]/div[1]/main[1]/div[1]/div[1]/div[2]/div[2]/div[1]/div[2]/div[" + i + "]")), 5000);
            await element.click();
        } catch (error) {
        }
    }
}

async function deleteFromCart() {
    const enrol_link_field = await driver.wait(until.elementLocated(By.linkText(elements.enrol_link)), 5000);
    await enrol_link_field.click();

    await driver.sleep(20000);
    const saveButton = await driver.wait(until.elementLocated(By.className(elements.delete_from_cart)), 5000);
    await driver.wait(until.elementIsVisible(saveButton)).click();

    await driver.sleep(10000);
}

async function bundlePage() {
    await driver.get(elements.bundle_page_url);
}

async function programList() {
    try {
        await driver.navigate().refresh();
        await driver.sleep(5000);
        var whole_div = await driver.findElements(By.css(elements.course_bundle_list));
    } catch (error) {

    }

    for (i = 1; i <= whole_div.length; i++) {
        await driver.get(elements.bundle_page_url);
        try {
            await driver.navigate().refresh();
            await driver.sleep(5000);
            const course_list = await driver.wait(until.elementLocated(By.xpath("/html[1]/body[1]/div[1]/div[1]/div[1]/div[1]/main[1]/div[1]/div[1]/div[2]/div[2]/div[1]/div[2]/div[" + i + "]")), 5000);
            await course_list.click();
        } catch (error) {

        }
    }
}

async function login_function() {
    const loginlink_field = await driver.wait(until.elementLocated(By.xpath(elements.login_link)), 10000);
    await loginlink_field.click();
    await driver.switchTo().defaultContent();
    const username_field = await driver.wait(until.elementLocated(By.id(elements.username)), 10000);
    await username_field.sendKeys(data.email_id);
    const passowrd_field = await driver.wait(until.elementLocated(By.id(elements.passowrd)), 10000);
    await passowrd_field.sendKeys(data.pwd);
    const login_btn_field = await driver.wait(until.elementLocated(By.id(elements.loginBtn)), 10000);
    await login_btn_field.click();
}

exports.homepage = homepage;
exports.navigate_to_course_catalogue = navigate_to_course_catalogue;
exports.catalogue_page_validation = catalogue_page_validation;
exports.short_course_filter = short_course_filter;
exports.firstCourse = firstCourse;
exports.frameSwitching = frameSwitching;
exports.courseList = courseList;
exports.programList = programList;
exports.deleteFromCart = deleteFromCart;
exports.bundlePage = bundlePage;
