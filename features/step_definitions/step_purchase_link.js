const webdriver = require('selenium-webdriver');
const { Before, Given, When, Then } = require('cucumber');
const { Builder, By, Key, until } = require("selenium-webdriver");
const seleniumWebDriver = require('selenium-webdriver');
const firefox = require('selenium-webdriver/firefox');
const course_enrol = require('../page_actions/page_purchase_links');

Given('user is on online domain home page', { timeout: 5 * 10000 }, async function () {
  await course_enrol.homepage();
});

When('they clicks the courses & degress link', { timeout: 5 * 10000 }, async function () {
  await course_enrol.navigate_to_course_catalogue();
});


Then('the user should get the course catalogue page', { timeout: 5 * 10000 }, async function () {
  await course_enrol.catalogue_page_validation();
});


Given('the user is on course catalogue page', { timeout: 5 * 10000 }, async function () {
  await course_enrol.short_course_filter();
});


When('they clicks the FS courses', { timeout: 50 * 100000 }, async function () {
  await course_enrol.firstCourse();
});



When('login into Marketplace', { timeout: 50 * 100000 }, async function () {
  await course_enrol.frameSwitching();
});


Then('they should be able to purchase the courses', { timeout: 50 * 100000 }, async function () {
  await course_enrol.courseList();
});



Then('the user able to remove the course from the cart', { timeout: 50 * 100000 }, async function () {
  await course_enrol.deleteFromCart();
});


Given('the user is on bundle page', { timeout: 50 * 100000 }, async function () {
  await course_enrol.bundlePage();
});


When('they clicks the FS program', { timeout: 50 * 100000 }, async function () {
  await course_enrol.programList();
});

Then('they should be able to purchase the bundles', { timeout: 50 * 100000 }, async function () {
  await course_enrol.deleteFromCart();
});
