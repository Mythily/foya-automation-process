var gulp = require('gulp');
var reporter = require('cucumber-html-reporter');

var options = {
        theme: 'bootstrap',
        jsonFile: './reports/cucumber-json-report.json',
        output: './reports/cucumber_report.html',
        reportSuiteAsScenarios: true,
        scenarioTimestamp: true,
        launchReport: true,
        metadata: {
            "App Version":"1.0.0",
            "Test Environment": "STAGING",
            "Browser": "Chrome  54.0.2840.98",
            "Platform": "Mac",
            "Parallel": "Scenarios",
            "Executed": "Remote"
        }
};

gulp.task('cucumberReports', function(){
    reporter.generate(options);
});